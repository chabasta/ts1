package cz.cvut.fel.ts1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChabastaTest {
    @Test
    public void factorialTest1(){
        Chabasta chabasta = new Chabasta();

        int n = 8;
        int expected =40320;
        int result = chabasta.factorialIterative(n);

        Assertions.assertEquals(expected, result);

    }
    @Test
    public void factorialTest2(){
        Chabasta chabasta = new Chabasta();

        int n = -5;
        int expected =0;
        int result = chabasta.factorialIterative(n);

        Assertions.assertEquals(expected, result);

    }
    @Test
    public void factorialTest3(){
        Chabasta chabasta = new Chabasta();

        int n = 0;
        int expected =1;
        int result = chabasta.factorialIterative(n);

        Assertions.assertEquals(expected, result);

    }
}
